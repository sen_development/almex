@echo off
echo INSTALL.BAT #65604 V1.0 13.02.2019 BO

rem Diese Batch laedt den Bootloader bestehend aus mini.bin und armboot.bin
rem und die Hardwarekonfigurationsdatei hwkonf.txt in ein HW4552/HW4553 mit
rem PXA320 Prozessor (Monahans).
rem Dazu werden die Hilfsprogramme bzw. Dateien
rem gjtag.exe    #58250
rem mkhwkonf.exe #33513
rem bmerge       #39259
rem nandobm.bin  #39453
rem reloc.bin    #45518
rem al110b00.bin #45519
rem ab der angegebenen HW-Save Nummer benoetig

rem 1. HWI Konfigurationsdatei in eine Binaerdatei wandeln
mkhwkonf hwkonf.txt hwkonf.bin
if NOT %ERRORLEVEL%==0 GOTO fehler_jtag_mkhwkonf

rem 2. BOOT.BIN aus den Dateien NANDOBM.BIN, MINI.BIN, HWKONF.BIN und
rem RELOC.BIN erstellen
bmerge /i nandobm.bin 0 /i mini.bin 8 /i hwkonf.bin 0x10008 /i reloc.bin 0x1f800 /r 0x20000 /o boot.bin
if NOT %ERRORLEVEL%==0 GOTO fehler_jtag_bmerge

rem 2. BOOT.BIN und ARMBOOT.BIN in das NAND-Flash brennen
gjtag /m 2 /dev 2 /dbgreg /nw /ic al110b00.bin /fn boot.bin 0 0xfc /fn armboot.bin 1 0xfb /bm 3
if NOT %ERRORLEVEL%==0 GOTO fehler_jtag_bootldr

echo Bootloader und HWI Block erfolgreich geladen!

goto ende

:fehler_jtag_bootldr
echo Fehler: Konnte Bootloader oder HWI Block nicht einspielen!
goto ende

:fehler_jtag_bmerge
echo Fehler: BMERGE fehlgeschlagen!
goto ende

:fehler_jtag_mkhwkonf
echo Fehler: MKHWKONF fehlgeschlagen!
goto ende

:ende

