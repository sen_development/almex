#ifndef __MSP430PWR_H__
#define __MSP430PWR_H__

/* MSP Commands */
#define MSP_CMD_GET_VERSION              0x01
#define MSP_CMD_GET_HWSAVE_NO            0x02
#define MSP_CMD_GET_MESSAGE_COUNTER      0x03
#define MSP_CMD_GET_BAT_STATUS           0x04
#define MSP_CMD_GET_BAT_CHRG_LEVEL       0x05
#define MSP_CMD_GET_BAT_CONFIG           0x06
#define MSP_CMD_GET_LIBAT_VOLT           0x07
#define MSP_CMD_GET_BATTERY_VOLTAGE      0x08
#define MSP_CMD_GET_ULTRACAPS_VOLTAGE    0x09
#define MSP_CMD_GET_CURRENT_STATES       0x0a
#define MSP_CMD_GET_DELTA_UBAT           0x0b
#define MSP_CMD_GET_UBAT_MAX             0x0c
#define MSP_CMD_GET_BAT_CAP              0x0d
#define MSP_CMD_GET_DEVICE_TEMP          0x0e
#define MSP_CMD_RETRIG_LIBAT_MEASURE     0x0f
#define MSP_CMD_RETRIG_TEMP_MEASURE      0x10
#define MSP_CMD_SET_WAKEUP_DELAY_L       0x11
#define MSP_CMD_SET_WAKEUP_MIN_BCAP      0x12
#define MSP_CMD_GET_WAKEUP_REASON        0x13
#define MSP_CMD_GET_VEXT_VOLT            0x14
#define MSP_CMD_PEEK                     0x15
#define MSP_CMD_POKE                     0x16
#define MSP_CMD_SET_HIGH_ADDR            0x17
#define MSP_CMD_SET_WAKEUP_DELAY_H       0x18
#define MSP_CMD_SET_PWR_ON               0x19
#define MSP_CMD_SET_CONTROL              0x1a
#define MSP_CMD_READ_I2C                 0x1b
#define MSP_CMD_WRITE_I2C                0x1c
#define MSP_CMD_GET_ADC                  0x1d
#define MSP_CMD_UPDATE_INIT              0x1e
#define MSP_CMD_UPDATE_START             0x1f
#define MSP_CMD_SET_WAKEUP_VOLTAGE       0x20
#define MSP_CMD_SET_BAT_ON               0x21
#define MSP_CMD_GET_BATINFO              0x22
#define MSP_CMD_GET_BATTEMP              0x23
#define MSP_CMD_GET_LIFETIME_CCAP        0x24
#define MSP_CMD_GET_PWR_ON               0x25
#define MSP_CMD_GET_BAT_ON               0x26

/* MSP Commands bit definitions */
#define TIMER_STATE_COLDSTART            0
#define TIMER_STATE_BATTERYCHECK         1
#define TIMER_STATE_EXT_POWER_ABOVE_18V  2
#define TIMER_STATE_EXT_POWER_BELOW_16V  3
#define TIMER_STATE_EXT_POWER_BELOW_7V   4
#define TIMER_STATE_EXT_POWER_RESUMED    5

#define CHARGER_STATE_COLDSTART          0
#define CHARGER_STATE_BAT_VOLTAGE_TEST   1
#define CHARGER_STATE_IDLE               2 
#define CHARGER_STATE_FASTCHARGE         3
#define CHARGER_STATE_TRICKLECHARGE      4
#define CHARGER_STATE_FULLCHARGED        5
#define CHARGER_STATE_CHARGE_INTERRUPT   6
#define CHARGER_STATE_PRECHARGE          7

#define CONTROL_DISABLE_TIMER            BIT0
#define CONTROL_DISABLE_CHARGE           BIT1
#define CONTROL_DISABLE_IGNITION         BIT2
#define CONTROL_DISABLE_BATHOLD          BIT3
#define CONTROL_RESET_ENDLESS            BIT8

#define MSP_BATTERY_TYPE_NONE            0
#define MSP_BATTERY_TYPE_ULTRACAPS       1
#define MSP_BATTERY_TYPE_NIMH            2

#define MSP_BATSTATUS_CHARGING           BIT0
#define MSP_BATSTATUS_INTERRUPTED        BIT1

#define BATCONF_SMART_PRESENT            BIT0
#define BATCONF_ROMID_OK                 BIT1
#define BATCONF_STRUCTURE_OK             BIT2
#define BATCONF_NEW_DATA                 BIT3
#define BATCONF_LCC_PRESENT              BIT4

#define MSP_WAKEUP_REASON_COLDBOOT       BIT0
#define MSP_WAKEUP_REASON_EXT_POWER_GOOD BIT1
#define MSP_WAKEUP_REASON_IGNITION_KEY   BIT2
#define MSP_WAKEUP_REASON_TIMER          BIT3
#define MSP_WAKEUP_REASON_VOLTAGE        BIT4

/* MSP communication timeouts */
#define MSP_WAIT_CWT_US                  150
#define MSP_WAIT_BWT_US                  250
#define MSP_WAIT_EWT_US                  (MSP_WAIT_BWT_US - MSP_WAIT_CWT_US)

/* MSP charger related constants */
#define MSP_CHARGE_LEVEL_NAH_PER_TICK_R1 48790
#define MSP_CHARGE_LEVEL_NAH_PER_TICK_R2 42500

#define MSP_CHARGE_CURRENT_MA            366
#define MSP_CHARGE_COUNTER_UPDATE_S      600
#define MSP_CHARGE_COUNTER_MAH_PER_TICK  (MSP_CHARGE_CURRENT_MA * MSP_CHARGE_COUNTER_UPDATE_S / 3600)

#endif

