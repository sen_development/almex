@echo off

rem *************************************************************
rem #63117 PTC7PRG.BAT V1.0 20.04.2016 TST
rem
rem This batch is used to programm PTC7 using debug connector.
rem 
rem Programming eqipment needed:
rem - T26590 "PRG90200 USB"
rem - T26591 "KABEL PROGRAMMIERADAPTER 90200"
rem
rem Programm packages required:
rem - #62706 gjtag.exe used to set/clear Reset and Erase  
rem - #63119 ptc7init.exe used to transfer ptc7 firmware
rem - ptc7_flash_c.bin the ptc7 firmware, for example #63118
rem *************************************************************

set KEY_NAME="HKEY_CURRENT_USER\SOFTWARE\Metric AG\gjtag"
set VALUE_NAME=Port
set ValueType=
set ValueValue=


rem *** Sofware exist? ******************************************

if not exist gjtag.exe (
   echo Program gjtag does not exist.
   goto exit
)

if not exist ptc7init.exe (
   echo Program ptc7init does not exist.
   goto exit
)

if not exist ptc7_flash_c.bin (
   echo Firmware ptc7_flash_c.bin does not exist.
   goto exit
)


rem *** Get serial port number and initialize Reset and Erase ***

gjtag /dev 2 /port auto /sysrst 0 /pout 0
if not %ERRORLEVEL%==0 (
    echo Error: gjtag initialize, serial port open or wrong gjtag version
    goto exit
)

FOR /F "usebackq skip=2 tokens=1,2*" %%A IN (`REG QUERY %KEY_NAME% /v %VALUE_NAME% 2^>nul`) DO (
    set ValueName=%%A
    set ValueType=%%B
    set /A ValueValue=%%C
)

@rem echo Value Name = %ValueName%
@rem echo Value Type = %ValueType%
@rem echo Value Value = %ValueValue%
set PortStr=.\\COM%ValueValue%

if NOT %ValueType%==REG_DWORD (
    echo Error: serial port registry type 
    goto exit
)

if %ValueValue% LEQ 0 (
    echo Error: serial port value 
    goto exit
)

echo.
echo PTC must be powered.

echo erase
gjtag /dev 2 /pout 1 > NUL
if not %ERRORLEVEL%==0 (
    echo Error: erase
    goto exit
)

echo reset
gjtag /dev 2 /pout 0 /sysrst 1 > NUL
if not %ERRORLEVEL%==0 (
    echo Error: reset
    goto exit
)

echo SAM-BA go
gjtag /dev 2 /sysrst 0 > NUL 
if not %ERRORLEVEL%==0 (
    echo Error: SAM-BA go
    goto exit
)


rem *** Programming *********************************************

ptc7init ptc7_flash_c.bin %PortStr%
if %ERRORLEVEL%==0 (
    goto done
)

ptc7init ptc7_flash_c.bin %PortStr%
if not %ERRORLEVEL%==0 (
    echo Error: programming
    goto exit
)

:done
echo.
echo DONE.
echo.

rem *** Exit ****************************************************

:exit
