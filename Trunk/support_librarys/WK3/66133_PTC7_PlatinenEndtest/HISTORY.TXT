History-Datei zu PTC7
=====================

Datum       Version Aenderung, Bemerkung usw.
14.03.2019  01.00f  MU  - Motortiming geaendert, wegen Resonanz im Schrittmotor
                        - wait_buffer_full(): Timeout vor Warteschleife berechnet. 
                          prnflags k�nnen sich au�erhalb der Warteschleife �ndern, 
                          wodurch pl�tzlich ein k�rzeres Timeout gelten kann, das dann 
                          zum Error f�hrt.
                        - Tabs entfernt
                        - Zeitliche Historie neueste �nderung nun oben
06.02.2018  01.00e  BO  Sensor Pullups werden auch eingeschaltet / 64769
20.03.2017  01.00d  BO  Status wird auch nach �ffnen des Deckels aktualisiert / 64160
25.11.2016  01.00c  BO  Kundendisplay gedreht / 63890
05.06.2016  01.00b  HT  Neu uebersetzt mit ARM-NONE-EABI 5.3.1 / 63324
12.05.2016  01.00a  HT  Automatische Anpassung der max. AAD an VIN / 63118
02.05.2016  00.00f  HT  CIS-A/D-Abfrage korrigiert / Scan funktioniert /
                    HT  Empfangspuffer fuer Debug-UART und USB getauscht /
                    BO  Anpassungen an anderen Compiler
26.04.2016  00.00e  HT  VIN statt VRPN abgefragt / 1. Druck funktioniert
21.04.2016  00.00d  HT  Display-Beleuchtung ok / kein Muell in Display-Mitte
20.04.2016  00.00c  HT  Tabelle der Ports bei Kommando 37 angepasst
19.04.2016  00.00b  HT  Neues Protokoll mit C/D / PTC7_RDY als Output konfig.
06.12.2015  00.00a  HT  Beginn der Erstellung aus PTC3 V. 03.05i / 22.02.2016
