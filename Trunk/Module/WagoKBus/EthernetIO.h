//////////////////////////////////////////////////////////////////////////////
//                                                                          //
//     (c) WAGO Kontakttechnik GmbH, Hansastra�e 27, D-32423 Minden         //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
//                                                                          //
// Projekt     :  WAGO I/O Bibliothek Serie 0750			                      //
//                                                                          //
// Zielsystem  :  Microsoft Windows XP mit Service Pack 3 und h�her         //
//                                                                          //
// IDE         :  Microsoft Visual Studio 2013, Service Pack 1              //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
//                                                                          //
// Datei       :  EthernetIO.h		                                          //
//                                                                          //
// Aufgabe     :  Definition  der Schnittstelle/ Exporte f�r die DLL		    //
//                                                                          //
// Bearbeiter  :  DS - Dennis Schmidt ( -77719 )                            //
//                                                                          //
// Version     :  2.0.0.00  -- 2005-12-05 DS -- Erstellung                  //
//                2.0.0.21  -- 2006-03-02 DS -- Erweiterter Registerbereich //
//                                              hinzugef�gt                 //
//                2.0.0.22  -- 2006-03-20 DS -- DIRd(), GetCouplerDiag() Fix//
//                2.0.0.23  -- 2006-03-31 DS -- Modbus Exception eingef�gt  //
//                2.0.0.24  -- 2006-06-08 DS -- KBus Reset Zeit erh�ht      //
//				        2.0.0.25	-- 2006-08-14 DS -- Zugriff auf > 256 Worte		  //
//				        2.0.0.26	-- 2006-08-24 DS -- WriteExtendedRegister()		  //
//												                      hinzugef�gt					        //
//                2.0.0.27  -- 2007-06-06 DS -- Bugfix 0x8403 KBus Ident    //
//                2.0.0.28  -- 2007-06-20 DS -- Tabellenbezogene Register-  //
//                                              kommunikation integriert    //
//				        2.0.0.29	-- 2007-11-08 DS -- "Coil" Zugriff hinzugef�gt  //
//                2.0.0.30  -- 2008-01-14 DS -- 0750-0482 als komplexe      //
//                                              Klemme behandeln (HART)     //
//                2.0.x.31  -- 2008-08-22 DS -- 200 Byte Framegrenze behoben//
//                2.0.x.32  -- 2009-02-02 DS -- UNICODE Kompatibilit�t      //
//                2.0.x.33  -- 2009-04-01 DS -- Synchronisation verbessert  //
//                2.0.x.34  -- 2009-05-15 DS -- Unterst�tzung von 16 DI/DO, //
//                                              0750-0484 ( HART ) erg�nzt  //
//                2.0.x.35  -- 2010-07-01 DS -- Ignoriere 631 FW Fehler     //
//                2.0.x.36  -- 2012-04-04 DS -- Komplexe Auswertung neu     //
//                2.0.x.37  -- 2012-04-11 DS -- Reset �berarbeitung         //
//                2.0.x.38  -- 2014-01-27 DS -- Reset �berarbeitung         //
//                2.0.x.39  -- 2014-06-23 DS -- Fehlerausgabe bei HART Modus//
//                2.0.x.40  -- 2014-08-18 DS -- Tabellenanzahl auf 8 erh�ht //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Include- W�chter
#ifndef __ETHERNETIO_H__
#define __ETHERNETIO_H__


//////////////////////////////////////////////////////////////////////////////
// Exportdefinitionen bei Verwendung von C++ Compilern
#ifndef __MBT_H__
  #ifdef	__cplusplus
    #define	DLLEXPORT extern "C"  __declspec( dllexport )
  #else
    #define	DLLEXPORT			        __declspec( dllexport )
  #endif
#endif

//////////////////////////////////////////////////////////////////////////////
// Zu Exportierende Datentypen

// CALLBACK f�r Registerkommunikation
typedef void ( CALLBACK *OnOperationDone )( void* pContext,
                                            BOOL  bWriteRegister,
                                            BYTE  byRegister );
#ifndef __IOSYSTEMDEFINES_H__                                        
  // Struktur zur Abfrage von Klemmeninformationen
  typedef struct _stIOInfo
  {
      WORD wKBusIdentCode;        // KBus Kennung
      TCHAR szDescription[ 32 ];  // Beschreibung der Klemme
      TCHAR szArticleNr[ 32 ];    // Artikelnummer lang
      TCHAR szSWVersion[ 8 ];     // Software Version
      TCHAR szHWVersion[ 8 ];     // Hardware Version
      BYTE bySlot;                // Steckplatznummer
      int  nType;                 // AnalogIO, ComplexIO, DigitalIO
      BYTE byChannels;            // Kanalanzahl
      WORD wBitPerChannel;        // Bits pro Kanal
      WORD wBitOffsetPAE;         // Bit- Offset im Eingangsabbild
      WORD wBitLengthPAE;         // Bit- L�nge im Eingangsabbild
      WORD wBitOffsetPAA;         // Bit- Offset im Ausgangsabbild
      WORD wBitLengthPAA;         // Bit- L�nge im Ausgangsabbild
  } stIOInfo;    

  // Klemmentyp ( Digital , Analog , Komplex , Beckhoff )
  enum ENIOType  { TYPE_DIGITALIO  = 256  ,
                   TYPE_ANALOGIO   = 512  ,
                   TYPE_COMPLEXIO  = 1024 ,
                   TYPE_BECKHOFFIO = 2048 };

  // Kopplertyp ( DHCP, BOOTP, EEPROM )
  enum ENFBCType { TYPE_BOOTP      = 1    ,
                   TYPE_DHCP       = 2    ,
                   TYPE_EEPROM     = 4    };

  // Tabellennummern ( Tabelle 0.. Tabelle 7 oder alle Tabellen )
  enum ENIOTable { TABLE_0         = 0   ,
                   TABLE_1         = 1   ,
                   TABLE_2         = 2   ,
                   TABLE_3         = 3   ,
                   TABLE_4         = 4   ,
                   TABLE_5         = 5   ,
                   TABLE_6         = 6   ,
                   TABLE_7         = 7   ,
                   TABLE_ALL       = 255 };
#endif


//////////////////////////////////////////////////////////////////////////
// Verbindungskontrolle

// Aufbau einer Verbindung zu einer IP- Adresse/ DNS- Name
DLLEXPORT long CALLBACK Connect   ( LPCTSTR lpszHostAddress,
                                    long *lSocket );

// Abbau einer Verbindung
DLLEXPORT long CALLBACK Disconnect( long lSocket );


//////////////////////////////////////////////////////////////////////////
// Zugriff auf das Prozessabbild �ber Ethernet- Feldbus ( Modbus TCP )

// Schreiben in das Prozessausgangsabbild
DLLEXPORT long CALLBACK PAAWr ( long lSocket,
                                WORD wOffset,
                                WORD wNumOfBytes,
                                LPBYTE pbyData );

// Lesen aus dem Prozesseingangsabbild
DLLEXPORT long CALLBACK PAERd ( long lSocket,
                                WORD wOffset,
                                WORD wNumOfBytes,
                                LPBYTE pbyData );

// Lesen aus dem Prozessausgangsabbild
DLLEXPORT long CALLBACK PAARd ( long lSocket,
                                WORD wOffset,
                                WORD wNumOfBytes,
                                LPBYTE pbyData );


// Setze Digitalausg�nge �ber "Coil" Zugriff
DLLEXPORT long CALLBACK CoilWr( long lSocket,
                                WORD wOffset,
                                WORD wNumOfBits,
                                LPBYTE pbyData );

// Lese Digitaleing�nge �ber "Coil" Zugriff
DLLEXPORT long CALLBACK CoilRd( long lSocket,
                                WORD wOffset,
                                WORD wNumOfBits,
                                LPBYTE pbyData );


//////////////////////////////////////////////////////////////////////////
// Zugriff auf Digitalklemmen

// Setze Digitalausg�nge
DLLEXPORT long CALLBACK DOWr  ( long lSocket,
                                BYTE bySlot,
                                BYTE byData,
                                BYTE byMask );

DLLEXPORT long CALLBACK DOWr16( long lSocket,
                                BYTE bySlot,
                                WORD wData,
                                WORD wMask );


// Lese Digitalausg�nge
DLLEXPORT long CALLBACK DORd  ( long lSocket,
                                BYTE bySlot,
                                LPBYTE pbyData );

DLLEXPORT long CALLBACK DORd16( long lSocket,
                                BYTE bySlot,
                                LPWORD pwData );

// Lese Digitaleing�nge inkl. Diagnoseeing�nge
DLLEXPORT long CALLBACK DIRd  ( long lSocket,
                                BYTE bySlot,
                                LPBYTE pbyData );

DLLEXPORT long CALLBACK DIRd16( long lSocket,
                                BYTE bySlot,
                                LPWORD pwData );

// Zur�cksetzen aller Digitalausg�nge
DLLEXPORT long CALLBACK ResetAllDO( long lSocket );


//////////////////////////////////////////////////////////////////////////
// Zugriff auf Analogklemmen

// Schreiben eines Prozesswertes in einen Kanal einer Analogausgangsklemme
DLLEXPORT long CALLBACK AOWr  ( long lSocket,
                                BYTE bySlot,
                                BYTE byChannel,
                                WORD wData );

// Lesen eines Prozesswertes aus einem Kanal einer Analogausgangsklemme
DLLEXPORT long CALLBACK AORd  ( long lSocket,
                                BYTE bySlot,
                                BYTE byChannel,
                                LPWORD pwData );

// Lesen eines Prozesswertes aus einem Kanal einer Analogeingangsklemme
DLLEXPORT long CALLBACK AIRd  ( long lSocket,
                                BYTE bySlot,
                                BYTE byChannel,
                                LPWORD pwData );


//////////////////////////////////////////////////////////////////////////
// Registerfunktionen

// Lesen von MODBUS Register
DLLEXPORT long CALLBACK ReadMBTRegister     ( long lSocket,
                                              WORD wStartAddress,
                                              WORD wNumOfRegister,
                                              LPWORD pwData );

// Schreiben von MODBUS Register
DLLEXPORT long CALLBACK WriteMBTRegister    ( long lSocket,
                                              WORD wStartAddress,
                                              WORD wNumOfRegister,
                                              LPWORD pwData );


// Lesen von Klemmenregistern aus einem Kanal einer Klemme
DLLEXPORT long CALLBACK ReadRegister        ( long lSocket,
                                              BYTE bySlot,
                                              BYTE byTable,
                                              BYTE byNumOfRegister,
                                              LPBYTE pbyRegisterNo,
                                              LPWORD pwData );

// Schreiben von Klemmenregistern in einen Kanal einer Klemme
DLLEXPORT long CALLBACK WriteRegister       ( long lSocket,
                                              BYTE bySlot,
                                              BYTE byTable,
                                              BYTE byNumOfRegister,
                                              LPBYTE pbyRegisterNo,
                                              LPWORD pwData );


// Lesen von Klemmenregistern aus allen Kan�len aller Klemmen eines Knotens
DLLEXPORT long CALLBACK ReadRegisterEx      ( long lSocket,
                                              BYTE byNumOfSlots,
                                              LPBYTE pbySlot,
                                              BYTE byNumOfRegister,
                                              LPBYTE pbyRegisterNo,
                                              LPWORD pwData,
                                              OnOperationDone lpfnRegisterCallback = NULL,
								                              void* pContext = 0 );

// Schreiben von Klemmenregistern in allen Kan�len aller Klemmen eines Knotens
DLLEXPORT long CALLBACK WriteRegisterEx     ( long lSocket,
                                              BYTE byNumOfSlots,
                                              LPBYTE pbySlot,
                                              BYTE byNumOfRegister,
                                              LPBYTE pbyRegisterNo,
                                              LPWORD pwData,
                                              OnOperationDone lpfnRegisterCallback = NULL,
							                                void* pContext = 0 );


// Lesen von Klemmenregistern aus einzelnen/allen Kan�len aller Klemmen eines Knotens
DLLEXPORT long CALLBACK ReadRegisterTableEx ( long lSocket,
                                              BYTE byNumOfSlots,
                                              LPBYTE pbySlot,
                                              BYTE byTable,
                                              BYTE byNumOfRegister,
                                              LPBYTE pbyRegisterNo,
                                              LPWORD pwData,
                                              OnOperationDone lpfnRegisterCallback = NULL,
								                              void* pContext = 0 );

// Schreiben von Klemmenregistern in allen Kan�len aller Klemmen eines Knotens
DLLEXPORT long CALLBACK WriteRegisterTableEx( long lSocket,
                                              BYTE byNumOfSlots,
                                              LPBYTE pbySlot,
                                              BYTE byTable,
                                              BYTE byNumOfRegister,
                                              LPBYTE pbyRegisterNo,
                                              LPWORD pwData,
                                              OnOperationDone lpfnRegisterCallback = NULL,
							                                void* pContext = 0 );


// Lesen von erweiterten Klemmenregistern �ber die Mailbox aus einer Klemme
DLLEXPORT long CALLBACK ReadExtendedRegister( long lSocket,
                                              BYTE bySlot,
                                              BYTE byTable,
                                              BYTE byTableType,
                                              WORD wNumOfRegister,
                                              LPWORD pwRegisterNo,
                                              LPLONG plData,
                                              LPLONG plCheckSum );

// Schreiben von erweiterten Klemmenregistern �ber die Mailbox in eine Klemme
DLLEXPORT long CALLBACK WriteExtendedRegister(long lSocket,
                                              BYTE bySlot,
                                              BYTE byTable,
                                              BYTE byTableType,
                                              LPLONG plData,
                                              BOOL bSetFactoryDefault = TRUE );


//////////////////////////////////////////////////////////////////////////
// Resetfunktionen

// KBus Reset
DLLEXPORT long CALLBACK ResetKBus       ( long lSocket );

// Software Reset
DLLEXPORT long CALLBACK ResetSW         ( long lSocket );

// Reset
DLLEXPORT long CALLBACK Reset           ( long lSocket );


//////////////////////////////////////////////////////////////////////////
// Diagnosefunktionen & Abfragen der Konfiguration

// Kopplerdiagnose ermitteln (Fehlercode & Fehlerargument)
DLLEXPORT long CALLBACK GetCouplerDiag  ( long lSocket,
                                          LPWORD pwCode,
                                          LPWORD pwArg );

// Klemmenkonfiguration des Koppler ermitteln
DLLEXPORT long CALLBACK GetConfig       ( long lSocket,
                                          LPBYTE pbyNumOfModules,
                                          LPWORD pwIdentCode );

// Pr�fen ob Koppler aktiv am Ethernet Feldbus verf�gbar ist
DLLEXPORT BOOL CALLBACK IsCouplerActive ( long lSocket,
                                          LPCTSTR lpszHostAddress );


//////////////////////////////////////////////////////////////////////////
// Klemmeninformationen abfragen

// Artikelnummer der Klemme
DLLEXPORT long CALLBACK GetArticleNr    ( long lSocket,
                                          BYTE bySlot,
                                          LPTSTR lpszArticleNr );

// Versionsstand der Klemme
DLLEXPORT long CALLBACK GetIOVersion    ( long lSocket,
                                          BYTE bySlot,
                                          LPTSTR lpszHW,
                                          LPTSTR lpszSW );

// Informationen zur Klemme
DLLEXPORT long CALLBACK GetIOInfo       ( long lSocket,
                                          BYTE bySlot,
                                          stIOInfo* pstIOInfo );


//////////////////////////////////////////////////////////////////////////
// Fehlermanagement

// Abrufen von Fehlerinformationen zum letzten Fehler
DLLEXPORT long CALLBACK GetLastIOError  ( long lSocket,
                                          LPTSTR lpszMsg );


//////////////////////////////////////////////////////////////////////////////
// Fehlercodes EthernetIO Bibliothek
#ifndef __IOSYSTEMERROR_H__
  // Ung�ltiger Socket bzw. kein Aufruf von Connect( )
  #define IO_INVALID_HANDLE_ERROR							( 0xE0010000 |  0UL )

  // Fehler beim Aufruf einer Funktion aus der MBT.dll
  #define IO_MBT_CALL_ERROR								    ( 0xE0010000 |  1UL )

  // Ung�ltiger Zeiger als Aufrufparameter
  #define IO_INVALID_POINTER_ERROR						( 0xE0010000 |  2UL )

  // Teilnehmer konnte nicht erreicht werden
  #define IO_DEVICE_NOT_REACHABLE_ERROR				( 0xE0010000 |  3UL )

  // Maximale Teilnehmerzahl erreicht
  #define IO_MAX_CONNECTION_ERROR							( 0xE0010000 |  4UL )

  // Offset und L�nge des Prozessabbildes unzul�ssig
  #define IO_PROCESS_IMAGE_LENGTH_ERROR				( 0xE0010000 |  5UL )

  // Funktion wird vom Kopplertyp nicht unterst�tzt 
  #define IO_NOT_YET_IMPLEMENTED_ERROR				( 0xE0010000 |  6UL )

  // Funktion wird von Klemmentyp nicht unterst�tzt 
  #define IO_FUNCTION_NOT_SUPPORTED_ERROR			( 0xE0010000 |  7UL )

  // Das lesen eines Registers ist fehlgeschlagen
  #define IO_REGISTER_ERROR	    						  ( 0xE0010000 |  8UL )

  // Ung�ltige Tabelle f�r diese Klemme
  #define IO_INVALID_TABLE_ERROR							( 0xE0010000 |  9UL )

  // Ung�ltige Anzahl zu lesender/ schreibender Register
  #define IO_INVALID_NUMSOFREGISTER_ERROR			( 0xE0010000 | 10UL )

  // Ung�ltige Registernummer
  #define IO_INVALID_REGISTERNO_ERROR					( 0xE0010000 | 11UL )

  // Ung�ltiger Kanal f�r diese Klemme
  #define IO_INVALID_CHANNEL_ERROR						( 0xE0010000 | 12UL )

  // Ung�ltiger Steckplatz
  #define IO_INCORRECT_SLOT_ERROR             ( 0xE0010000 | 13UL )

  // Kein Speicher verf�gbar
  #define IO_NO_MEMORY_ERROR                  ( 0xE0010000 | 14UL )

  // Mailbox- Kommando Fehler
  #define IO_MAILBOX_CMD_ERROR                ( 0xE0010000 | 15UL )

  // Mailbox- Aktivierung Fehler
  #define IO_MAILBOX_ACTIVATE_ERROR                ( 0xE0010000 | 16UL )
  
  // TimeOut beim Kopieren von Daten vom RAM ins EEPROM
  #define IO_RAM_EEPROM_TIMEOUT							  ( 0xE0010000 | 17UL )

  // Umstellung des Kopplers auf 'Komplexe Auswertung
  #define IO_CHANGE_MODE_FAILED							  ( 0xE0010000 | 18UL )
#endif

///////////////////////////////////////////////////////////////////////////////
#endif