﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="Mein Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="CompassRepin.lvclass" Type="LVClass" URL="../CompassRepin.lvclass"/>
		<Item Name="Dataset.ctl" Type="VI" URL="../TypeDef/Dataset.ctl"/>
		<Item Name="MachineData.ctl" Type="VI" URL="../TypeDef/MachineData.ctl"/>
		<Item Name="TestData.ctl" Type="VI" URL="../TypeDef/TestData.ctl"/>
		<Item Name="TestStepData.ctl" Type="VI" URL="../TypeDef/TestStepData.ctl"/>
		<Item Name="UutData.ctl" Type="VI" URL="../TypeDef/UutData.ctl"/>
		<Item Name="Abhängigkeiten" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
			</Item>
		</Item>
		<Item Name="Build-Spezifikationen" Type="Build"/>
	</Item>
</Project>
